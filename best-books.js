// create api-key.js file with const API_KEY="your_api_key" in this same directory to use


//change the url based on the user input

//1. get the input of year, month, date from the html (date to request the bestsellers for in best selling hardcover nonfiction)

const submit = document.getElementById('form')

   .addEventListener('submit',function(e) {

    e.preventDefault();

        const year= document.getElementById('year').value;
        const month = document.getElementById('month').value;
        const date = document.getElementById('date').value;

//2. insert the date into the url and obtain results
        var BASE_URL = `https://api.nytimes.com/svc/books/v3/lists/${year}-${month}-${date}/hardcover-fiction.json?api-key=`;
        var url = `${BASE_URL}${API_KEY}`; //modify this to obtain intended results


//3. with results display the first 5 books (title / author / description)
fetch(url)
  .then(function(response) {
    return response.json(); 
  })
  .then(function(responseJson) {
    for (let i = 0; i < 5; i++) {
    var bookTitle = responseJson.results.books[i].title
    var bookAuthor = responseJson.results.books[i].author
    var bookDescription = responseJson.results.books[i].description
    
    document.getElementById(`books-container${i}`).innerHTML = `Title: ${bookTitle} By: ${bookAuthor} Description: ${bookDescription}`
 
    
    };
  });
});