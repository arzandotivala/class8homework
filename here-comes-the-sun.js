// Using requestAnimationFrame, animate the background-color of the body so that it changes from black rgb(0, 0, 0) to white rgb(255, 255, 255).  It should increment by 1 every frame.
// Once the background-color is rgb(255, 255, 255), should not call requestAnimationFrame again.
// It should take approximately 4-5 seconds for the background to animate from black to white.
//determines timeout on its own 

//Assignment 
let C1 = 0;
let a = document.getElementById('background');

const sunComes = function() {
    
    if(a.style.backgroundColor === `rgb(255, 255, 255)`) {
        return;     
    };
    if(a.style.backgroundColor != `rgb(255, 255, 255)`) {
         //execute color change to white 
         C1 = C1+1;
         a.style.backgroundColor = `rgb(${C1},${C1},${C1})`;
        requestAnimationFrame(sunComes)};
    };
    requestAnimationFrame(sunComes);

//EC
let C2 = 255;
let b = document.getElementById('text');

const changeText = function() {
    
    if(b.style.color === `rgb(0, 0, 0)`) {
        return;
    };
    if(b.style.color != `rgb(0, 0, 0)`) {
        //execute color change to black
        C2 = C2-1;
        b.style.color = `rgb(${C2},${C2},${C2})`;
        requestAnimationFrame(changeText)};
    };
    requestAnimationFrame(changeText);

